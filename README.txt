Issue assign 1.x for Drupal 6.x
-------------------------------

This module provides new permissions for the project_issue module. One
permission grants the rights to assign issues to others. The second allows
the issue to be assigned to the user by another person. Another permission
allows the given role's users to assign an issue to *any* user on the
system.

In any case, users may still assign an issue to themselves, or make an issue
unassigned, which is the default behavior of the project_issue module.

The permissions in this module do not interfere with "assign and be assigned
project issues" permission in the project_issue module. It simply alters the
project_issue_assignees hook and adds additional names to the drop-down
list based on the new permissions.

For more information about how this module works, see http://drupal.org/node/4354.

Installation
------------

Issue assign can be installed like any other Drupal module -- place it in
the modules directory for your site and enable it along with project_issue
and its required modules.
